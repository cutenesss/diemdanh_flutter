import 'package:diemdanh/config_assets/images.dart';

class ItemLogin {
  late final String label;
  late final String hint;

  ItemLogin(this.label, this.hint);
}

List<ItemLogin> loginData = [
  ItemLogin("Tài khoản", "Tài khoản"),
  ItemLogin("Mật khẩu", "Mật khẩu"),
];

const String UNAUTHORIZED_USERNAME_NOT_FOUND = "UNAUTHORIZED_USERNAME_NOT_FOUND";
const String UNAUTHORIZED_WRONG_PASSWORD = "UNAUTHORIZED_WRONG_PASSWORD";

class ItemConstantListHome {
  late final String title;
  late final String icon;

  ItemConstantListHome(this.title, this.icon);
}

List<ItemConstantListHome> listItemHome = [
  ItemConstantListHome("Xin phép nghỉ làm", ImageAssets.iconNghiLam),
  ItemConstantListHome("Xin phép đến muộn", ImageAssets.iconDenMuon),
  ItemConstantListHome("Xin phép về sớm", ImageAssets.iconVeSom),
  ItemConstantListHome("Xin làm thêm", ImageAssets.iconLamThem),
  ItemConstantListHome("Xin OT", ImageAssets.iconOt),
];

List<String> overallNghiLam = [
  "Tổng nghỉ tuần", "Tổng nghỉ tháng"
];