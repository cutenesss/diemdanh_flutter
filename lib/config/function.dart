import 'package:flutter/material.dart';
import 'package:get/get.dart';

void showLoading(BuildContext context) {
  AlertDialog alert = AlertDialog(
    content: Row(
      children: [
        const CircularProgressIndicator(),
        Container(
            margin: const EdgeInsets.only(left: 10),
            child: const Text("Vui lòng đợi...")),
      ],
    ),
  );
  showDialog(
    barrierDismissible: false,
    useRootNavigator: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

void popupOk(String title, String content, Function? onPressOk, String? okTitle,
    Function? onPressCancel) {
  // set up the button
  Widget okButton = ElevatedButton(
    child: Text(okTitle ?? "Ok"),
    onPressed: () {
      if(onPressOk != null){
        onPressOk();
      }
      Get.back();
    },
  );
  Get.defaultDialog(
      title: title,
      middleText: content,
      backgroundColor: Colors.white,
      titleStyle: const TextStyle(color: Colors.black),
      middleTextStyle: const TextStyle(color: Colors.black),
      barrierDismissible: false,
      radius: 8,
      actions: [okButton]);
}
