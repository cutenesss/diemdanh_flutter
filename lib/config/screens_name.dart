class NamedAppRoute {
  static const String introRoute = '/';
  static const String loginRoute = '/login';
  static const String tabHomeRoute = '/tab_home';
  static const String xinPhepNghiLam = '/xin_phep_nghi_lam';
}