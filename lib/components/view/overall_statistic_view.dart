import 'package:diemdanh/config_assets/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OverallStatisticView extends StatelessWidget {
  List<String> dataTitle;
  List<int> dataContent;
  OverallStatisticView({Key? key, required this.dataTitle, required this.dataContent}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 12, 0, 12),
      decoration: const BoxDecoration(
          border: Border(
        bottom: BorderSide(
          color: Color(ColorAssets.colorBFC9DD),
          width: 1.0,
          style: BorderStyle.solid,
        ),
      )),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _itemText(dataTitle[0], dataContent[0].toString()),
          _itemText(dataTitle[1], dataContent[1].toString()),
        ],
      ),
    );
  }

  Widget _itemText(String title, String content) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: Text(
            title,
            style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          ),
        ),
        Text(content, style: const TextStyle(fontSize: 14))
      ],
    );
  }
}
