import 'package:diemdanh/config_assets/colors.dart';
import 'package:diemdanh/config_assets/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HeaderBack extends StatelessWidget implements PreferredSizeWidget {
  final Color backgroundColor = const Color(ColorAssets.colorEEF3FA);
  final Text title;
  final AppBar appBar;
  final List<Widget> widgets;

  const HeaderBack(
      {Key? key,
      required this.title,
      required this.appBar,
      required this.widgets})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backwardsCompatibility: false,
      systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Color(ColorAssets.primaryColor)),
      title: title,
      backgroundColor: backgroundColor,
      leadingWidth: 24, // <-- Use this
      centerTitle: false, // <-- and this
      leading: Container(
        margin: const EdgeInsets.only(left: 10),
        child: IconButton(
          icon: const Icon(Icons.arrow_back_ios, color: Colors.black, size: 20,),
          onPressed: () => Navigator.of(context).pop(),
        )
      ),
      actions: [
        Container(
            margin: const EdgeInsets.only(right: 12),
            child: Image.asset(
              ImageAssets.companyLogo,
              fit: BoxFit.contain,
              width: 90,
            )),
        ...widgets],
      titleTextStyle: const TextStyle(
          color: Colors.black, fontWeight: FontWeight.w500, fontSize: 16),
      elevation: 0.0,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(50);
}
