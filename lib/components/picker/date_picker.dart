import 'package:diemdanh/config_assets/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:intl/intl.dart';

class DatePicker extends StatefulWidget {
  final String title;

  const DatePicker({Key? key, required this.title}) : super(key: key);

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  /// Which holds the selected date
  /// Defaults to today's date.
  DateTime selectedDate = DateTime.now();

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(1900),
      lastDate: DateTime(2100),
      helpText: 'CHON_NGAY'.tr,
      // Can be used as title
      cancelText: 'HUY'.tr,
      confirmText: 'OK'.tr,
    );
    if (picked != null && picked != selectedDate) {
      debugPrint(picked.toIso8601String());
      setState(() {
        selectedDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.fromLTRB(16, 10, 16, 10),
        child: Column(
          children: <Widget>[
            Row(
              children: [
                Text(
                  widget.title,
                  style: const TextStyle(fontSize: 15),
                ),
                Text(
                  '*',
                  style: TextStyle(fontSize: 15, color: Colors.red[400]),
                )
              ],
            ),
            GestureDetector(
              onTap: () => _selectDate(context),
              child: Container(
                margin: const EdgeInsets.only(top: 10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 1,
                        blurRadius: 1,
                        offset:
                            const Offset(0, 1), // changes position of shadow
                      )
                    ],
                    borderRadius: const BorderRadius.all(Radius.circular(8))),
                child: Container(
                    padding: const EdgeInsets.fromLTRB(12, 14, 12, 14),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(DateFormat('dd/MM/yyyy').format(selectedDate)),
                        const Icon(
                          Icons.date_range,
                          size: 17,
                          color: Color(ColorAssets.colorBFC9DD),
                        )
                      ],
                    )),
              ),
            )
          ],
        ));
  }
}
