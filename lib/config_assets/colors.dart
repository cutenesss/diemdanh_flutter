import 'package:diemdanh/config/setting.dart';

class ColorAssets {
  static const primaryColor = mainColor;
  static const indigo1A75BC = 0xFF1A75BC;
  static const color003264 = 0xFF003264;
  static const colorBFC9DD = 0xFFBFC9DD;
  static const color39476A = 0xFF39476A;
  static const colorGrayLabel = 0xFF3D4B58;
  static const colorFDEFF0 = 0xFFFDEFF0;
  static const colorBackground = 0xFFEBF0F6;
  static const colorF3F5F9 = 0xFFF3F5F9;
  static const colorEEF3FA = 0xFFEEF3FA;
}
