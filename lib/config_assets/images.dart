class ImageAssets {
  static const introImage = "assets/images/introImage.png";
  static const imageLogin = "assets/images/imageLogin.png";
  static const companyLogo = "assets/images/companyLogo.png";

  static const iconNghiLam = "assets/images/iconTienIch/iconNghiLam.svg";
  static const iconDenMuon = "assets/images/iconTienIch/iconDenMuon.svg";
  static const iconVeSom = "assets/images/iconTienIch/iconVeSom.svg";
  static const iconLamThem = "assets/images/iconTienIch/iconLamThem.svg";
  static const iconOt = "assets/images/iconTienIch/iconOt.svg";
}