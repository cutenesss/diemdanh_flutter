import 'package:diemdanh/config_assets/colors.dart';
import 'package:diemdanh/screens/login/login.dart';
import 'package:diemdanh/screens/tab_home/binding/tab_home_binding.dart';
import 'package:diemdanh/screens/tab_home/tab_home.dart';
import 'package:diemdanh/utils/secure_storage.dart';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import '../../config_assets/images.dart';

class Intro extends StatefulWidget {
  const Intro({
    Key? key,
  }) : super(key: key);

  @override
  _IntroState createState() => _IntroState();
}

class _IntroState extends State<Intro> {
  bool _visible = false;

  @override
  void initState() {
    super.initState();
    Timer.periodic(const Duration(milliseconds: 2500), (timer) {
      if (mounted) {
        setState(() {
          _visible = true;
        });
      }
    });
    onCheckRoute();
  }

  onCheckRoute () async {
    var userFromLocal =
    await SecureStorage.getObject(SecureStorage.keyUser());
    // debugPrint(userFromLocal);
    if (_visible == false) {
      Timer.periodic(const Duration(milliseconds: 4500), (timer) {
        if (mounted) {
          if(userFromLocal != null){
            Get.off(() => const TabHome(), binding: TabHomeBinding());
          } else {
            Get.off(() => Login());
            // Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute<void>(builder: (BuildContext context) => Login()), ModalRoute.withName(NamedAppRoute.loginRoute));
          }
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(ColorAssets.indigo1A75BC),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _itemChildren(Container(
            width: 200,
            decoration: const BoxDecoration(color: Colors.transparent),
            child: Image.asset(ImageAssets.introImage, fit: BoxFit.fitWidth),
          )),
          _itemChildren(Container(
              padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
              decoration: const BoxDecoration(color: Colors.transparent),
              child: Text(
                'APP_NAME'.tr,
                style: styleText,
              )))
        ],
      ),
    );
  }

  Widget _itemChildren(Widget child) {
    return Center(
      child: AnimatedOpacity(
        opacity: _visible ? 1.0 : 0.0,
        duration: const Duration(milliseconds: 1500),
        child: child,
      ),
    );
  }
}

const styleText =
    TextStyle(fontSize: 24, color: Colors.white, fontWeight: FontWeight.bold);
