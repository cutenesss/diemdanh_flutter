import 'package:diemdanh/config/screens_name.dart';
import 'package:diemdanh/config_assets/language/localization_service.dart';
import 'package:diemdanh/screens/intro/intro.dart';
import 'package:diemdanh/screens/login/login.dart';
import 'package:diemdanh/screens/tab_home/binding/tab_home_binding.dart';
import 'package:diemdanh/screens/tab_home/tab_home.dart';
import 'package:diemdanh/screens/tab_home/utility/xin_phep_nghi_lam/xin_phep_nghi_lam.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: GetMaterialApp(
        locale: LocalizationService().locale,
        fallbackLocale: LocalizationService.fallbackLocale,
        translations: LocalizationService(),
        initialRoute: NamedAppRoute.introRoute,
        // onGenerateRoute: _routes(),
        getPages: [
          GetPage(name: NamedAppRoute.introRoute, page: () => const Intro()),
          GetPage(name: NamedAppRoute.loginRoute, page: () => Login()),
          GetPage(
              name: NamedAppRoute.tabHomeRoute, page: () => const TabHome(), binding: TabHomeBinding()),
          GetPage(name: NamedAppRoute.xinPhepNghiLam, page: () => const XinPhepNghiLam()),
        ],
      ),
    );
  }

  RouteFactory _routes() {
    return (settings) {
      final Object? arguments = settings.arguments;
      Widget screens;
      switch (settings.name) {
        case NamedAppRoute.introRoute:
          screens = const Intro();
          break;
        case NamedAppRoute.loginRoute:
          screens = Login();
          break;
        case NamedAppRoute.tabHomeRoute:
          screens = const TabHome();
          break;
        default:
          return null;
      }
      return MaterialPageRoute(builder: (BuildContext context) => screens);
    };
  }
}
