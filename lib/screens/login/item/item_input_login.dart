import 'package:diemdanh/config_assets/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// ignore: must_be_immutable
class ItemInputLogin extends StatefulWidget {
  final String content = "";
  String label = "";
  String hint = "";
  bool? isPassword = false;
  bool isNumber = false;
  Function onChangeText;
  int? length;
  String? initValue;
  int lines = 1;

  ItemInputLogin(
      {Key? key,
      required this.label,
      required this.hint,
      required this.isPassword,
      required this.onChangeText})
      : super(key: key);

  @override
  _ItemInputLoginState createState() => _ItemInputLoginState();
}

class _ItemInputLoginState extends State<ItemInputLogin> {
  late FocusNode myFocusNode;
  late TextEditingController textController = TextEditingController();

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
    textController = TextEditingController(text: widget.initValue ?? "");
  }

  textListener(String text) {
    widget.onChangeText(widget.isPassword, text);
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    textController.dispose();
    super.dispose();
  }

  void _requestFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(myFocusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 25),
        child: TextField(
          onChanged: textListener,
          focusNode: myFocusNode,
          onTap: _requestFocus,
          maxLines: widget.lines,
          controller: textController,
          maxLength: widget.length ?? 255,
          inputFormatters: [
            LengthLimitingTextInputFormatter(widget.length),
          ],
          obscureText: widget.isPassword ?? false,
          keyboardType:
              widget.isNumber ? TextInputType.number : TextInputType.text,
          decoration: InputDecoration(
            counterText: "",
            contentPadding: const EdgeInsets.fromLTRB(12, 18, 12, 18),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            isDense: true,
            prefixIcon: Icon(
              widget.isPassword != null && widget.isPassword == true
                  ? Icons.lock
                  : Icons.account_circle,
              color: myFocusNode.hasFocus
                  ? const Color(ColorAssets.color003264)
                  : const Color(ColorAssets.color39476A),
            ),
            prefixIconConstraints: const BoxConstraints(
              minHeight: 40,
              minWidth: 40,
            ),
            labelText: widget.label,
            labelStyle: TextStyle(
                color: myFocusNode.hasFocus
                    ? const Color(ColorAssets.color003264)
                    : const Color(ColorAssets.color39476A),
                fontSize: 16,
                fontWeight: FontWeight.w500),
            hintText: widget.hint,
            hintStyle: TextStyle(fontSize: 16, color: Colors.grey[400]),
            enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                  color: Color(ColorAssets.colorBFC9DD), width: 1.0),
              borderRadius: BorderRadius.circular(8),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                  color: Color(ColorAssets.primaryColor), width: 1.0),
              borderRadius: BorderRadius.circular(8),
            ),
          ),
          autofocus: false,
        ));
  }
}
