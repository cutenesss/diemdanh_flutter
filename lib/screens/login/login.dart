import 'package:diemdanh/api/model/user/user_response_data.dart';
import 'package:diemdanh/api/user.dart';
import 'package:diemdanh/config/constant.dart';
import 'package:diemdanh/config/function.dart';
import 'package:diemdanh/config_assets/colors.dart';
import 'package:diemdanh/config_assets/images.dart';
import 'package:diemdanh/screens/tab_home/binding/tab_home_binding.dart';
import 'package:diemdanh/screens/tab_home/tab_home.dart';
import 'package:diemdanh/utils/secure_storage.dart';
import 'package:diemdanh/screens/login/item/item_input_login.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

// ignore: must_be_immutable, use_key_in_widget_constructors
class Login extends StatelessWidget {
  late String username = "";
  late String password = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Column(
        children: [
          viewImageCompany(),
          _listInput(),
          ElevatedButton(
            style: styleButton,
            onPressed: () => onLogin(context, username, password),
            child: Text('DANG_NHAP'.tr),
          ),
          viewImageLogin(),
        ],
      ),
    );
  }

  onLogin(BuildContext context, String username, String password) async {
    if (username == "" || password == "") {
      popupOk("THONG_BAO".tr, "VUI_LONG_DIEN_THONG_TIN".tr, null, null, null);
    } else {
      showLoading(context);
      try {
        final response =
            await UserApi().login({'username': username, 'password': password});
        SecureStorage.saveObject(
            SecureStorage.keyUser(),
            UserResponseData.fromJson(response)
                .userInformationResponse!
                .user
                .toString());
        SecureStorage.saveObject(
            SecureStorage.keyToken(),
            UserResponseData.fromJson(response)
                    .userInformationResponse!
                    .token ??
                "");
        // debugPrint(
        //     'a ${UserResponseData.fromJson(response).userInformationResponse!.token}');
        Navigator.of(context).pop();
        Get.off(() => const TabHome(), binding: TabHomeBinding());
        // Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute<void>(builder: (BuildContext context) => const TabHome()), ModalRoute.withName(NamedAppRoute.tabHomeRoute));
      } catch (e) {
        Navigator.of(context).pop();
        switch (e.toString()) {
          case UNAUTHORIZED_USERNAME_NOT_FOUND:
            popupOk("THONG_BAO".tr, "SAI_TEN_DANG_NHAP".tr, null, null, null);
            break;
          case UNAUTHORIZED_WRONG_PASSWORD:
            popupOk("THONG_BAO".tr, "SAI_MAT_KHAU".tr, null, null, null);
            break;
          default:
            popupOk("THONG_BAO".tr, "DA_CO_LOI_XAY_RA".tr, null, null, null);
            break;
        }
        // debugPrint('Error login: ${e.toString()}');
      }
    }
  }

  onChangeText(isPassword, text) {
    if (isPassword == true) {
      password = text;
    } else {
      username = text;
    }
  }

  ListView _listInput() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: loginData.length,
      itemBuilder: (context, int index) {
        return ItemInputLogin(
            label: loginData[index].label,
            hint: loginData[index].hint,
            isPassword: index == 0 ? false : true,
            onChangeText: onChangeText);
      },
    );
  }
}

Widget viewImageLogin() {
  return Container(
    width: 330,
    margin: const EdgeInsets.fromLTRB(0, 50, 0, 0),
    child: Image.asset(
      ImageAssets.imageLogin,
      fit: BoxFit.contain,
    ),
  );
}

Widget viewImageCompany() {
  return Container(
    margin: const EdgeInsets.fromLTRB(0, 120, 0, 30),
    child: Image.asset(
      ImageAssets.companyLogo,
      fit: BoxFit.contain,
      width: 200,
    ),
  );
}

ButtonStyle styleButton = ElevatedButton.styleFrom(
    textStyle: const TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
    primary: const Color(ColorAssets.primaryColor),
    padding: const EdgeInsets.fromLTRB(90, 12, 90, 12),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(18), // <-- Radius
    ));
