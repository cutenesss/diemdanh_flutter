import 'package:diemdanh/config_assets/colors.dart';
import 'package:diemdanh/screens/tab_home/home/home.dart';
import 'package:diemdanh/screens/tab_home/notifications/notification.dart';
import 'package:diemdanh/screens/tab_home/personal/personal.dart';
import 'package:diemdanh/screens/tab_home/utility/utility.dart';
import 'package:diemdanh/utils/secure_storage.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:flutter/material.dart';

class TabHome extends StatefulWidget {
  const TabHome({Key? key}) : super(key: key);

  @override
  _TabContainerState createState() => _TabContainerState();
}

class _TabContainerState extends State<TabHome> {
  late List<Widget> originalList;
  late Map<int, bool> originalDic;
  late List<Widget> listScreens;
  late List<int> listScreensIndex;
  int tabIndex = 0;
  Color tabColor = const Color(ColorAssets.colorBFC9DD);
  Color selectedTabColor = const Color(ColorAssets.primaryColor);

  @override
  void initState() {
    super.initState();
    originalList = [
      const Home(),
      const Notifications(),
      const Utility(),
      const Personal()
    ];
    originalDic = {0: true, 1: false, 2: false, 3: false};
    listScreens = [originalList.first];
    listScreensIndex = [0];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
          index: listScreensIndex.indexOf(tabIndex), children: listScreens),
      bottomNavigationBar: _buildTabBar(),
      backgroundColor: Theme.of(context).primaryColor,
    );
  }

  void _selectedTab(int index) {
    if (originalDic[index] == false) {
      listScreensIndex.add(index);
      originalDic[index] = true;
      listScreensIndex.sort();
      listScreens = listScreensIndex.map((index) {
        return originalList[index];
      }).toList();
    }
    setState(() {
      tabIndex = index;
    });
  }

  Widget _buildTabBar() {
    var listItems = [
      BottomAppBarItem(iconData: Icons.apartment, text: 'TRANG_CHU'.tr),
      BottomAppBarItem(
          iconData: Icons.notification_important, text: 'THONG_BAO'.tr),
      BottomAppBarItem(iconData: Icons.folder, text: 'TIEN_ICH'.tr),
      BottomAppBarItem(iconData: Icons.account_circle, text: 'CA_NHAN'.tr),
    ];

    var items = List.generate(listItems.length, (int index) {
      return _buildTabItem(
        item: listItems[index],
        index: index,
        onPressed: _selectedTab,
      );
    });

    return BottomAppBar(
      elevation: 0.0,
      child: Container(
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: items,
          )),
      color: Colors.white,
    );
  }

  Widget _buildTabItem({
    required BottomAppBarItem item,
    required int index,
    required ValueChanged<int> onPressed,
  }) {
    var color = tabIndex == index ? selectedTabColor : tabColor;
    var backgroundColor =
        tabIndex == index ? const Color(ColorAssets.colorFDEFF0) : Colors.white;
    bool isVisible = tabIndex == index ? true : false;
    var marginRight = tabIndex == index ? 5.0 : 0.0;
    return Container(
      padding: const EdgeInsets.fromLTRB(5, 8, 5, 8),
      child: GestureDetector(
          onTap: () => onPressed(index),
          child: AnimatedContainer(
              duration: const Duration(milliseconds: 700),
              curve: Curves.linear,
              padding: const EdgeInsets.fromLTRB(13, 10, 13, 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25.0),
                color: backgroundColor,
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  AnimatedContainer(
                      duration: const Duration(milliseconds: 700),
                      margin: EdgeInsets.fromLTRB(0.0, 0.0, marginRight, 0.0),
                      child: Icon(item.iconData, color: color, size: 22)),
                  _viewLabel(isVisible, color, item.text)
                ],
              ))),
    );
  }

  Widget _viewLabel(bool isVisible, var color, String text) {
    if (isVisible) {
      return Text(
        text,
        style: TextStyle(color: color, fontSize: 12),
      );
    } else {
      return const SizedBox.shrink();
    }
  }
}

class BottomAppBarItem {
  IconData iconData;
  String text;

  BottomAppBarItem({required this.iconData, required this.text});
}
