import 'package:diemdanh/components/header/header_back.dart';
import 'package:diemdanh/components/picker/date_picker.dart';
import 'package:diemdanh/components/view/overall_statistic_view.dart';
import 'package:diemdanh/config/constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

class XinPhepNghiLam extends StatelessWidget {
  const XinPhepNghiLam({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HeaderBack(
        title: Text('XIN_PHEP_NGHI_LAM'.tr),
        appBar: AppBar(),
        widgets: <Widget>[],
      ),
      body: ListView(
        children: [
          OverallStatisticView(dataTitle: overallNghiLam, dataContent: [0, 1]),
          DatePicker(title: 'NGAY'.tr)
        ],
      ),
    );
  }
}
