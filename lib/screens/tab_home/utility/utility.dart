import 'package:diemdanh/config/constant.dart';
import 'package:diemdanh/config_assets/colors.dart';
import 'package:diemdanh/screens/tab_home/item/item_list_home.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

class Utility extends StatelessWidget implements PreferredSizeWidget {
  const Utility({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(ColorAssets.colorBackground),
        appBar: AppBar(
          title: Text("TIEN_ICH".tr),
          backgroundColor: Colors.white,
          backwardsCompatibility: false,
          titleTextStyle: const TextStyle(
              color: Colors.black, fontWeight: FontWeight.w500, fontSize: 20),
          elevation: 0.0,
          bottom: PreferredSize(
              child: Container(
                color: const Color(ColorAssets.colorBFC9DD),
                height: 1.0,
              ),
              preferredSize: const Size.fromHeight(1.0)),
        ),
        body: Container(
            margin: const EdgeInsets.only(top: 10),
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: listItemHome.length,
              itemBuilder: (context, int index) {
                return ItemListHome(
                  title: listItemHome[index].title,
                  icon: listItemHome[index].icon,
                  index: index,
                  listDataLength: listItemHome.length - 1,
                );
              },
            )));
  }

  @override
  Size get preferredSize => const Size.fromHeight(52);
}
