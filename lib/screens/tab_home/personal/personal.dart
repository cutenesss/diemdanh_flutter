import 'package:diemdanh/config/function.dart';
import 'package:diemdanh/config_assets/colors.dart';
import 'package:diemdanh/screens/intro/intro.dart';
import 'package:diemdanh/utils/secure_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

class Personal extends StatelessWidget {
  const Personal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
        body:Center(
          child: ElevatedButton(
            style: styleButton,
            onPressed: () => onButtonLogOut(context),
            child: Text('DANG_XUAT'.tr),
          ),
        ),
      );
  }

  onButtonLogOut(BuildContext context) async {
    // showLoading(context);
    try {
      await SecureStorage.deleteObject(SecureStorage.keyUser());
      await SecureStorage.deleteObject(SecureStorage.keyToken());
      // Navigator.of(context).pop();
      Get.off(() => const Intro());
      // Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute<void>(builder: (BuildContext context) => const Intro()), ModalRoute.withName(NamedAppRoute.introRoute));
    } catch (e) {
      // Navigator.of(context).pop();
      debugPrint('Error logout: $e');
    }
  }
}

ButtonStyle styleButton = ElevatedButton.styleFrom(
    textStyle: const TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
    primary: const Color(ColorAssets.primaryColor),
    padding: const EdgeInsets.fromLTRB(90, 12, 90, 12),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(18), // <-- Radius
    ));