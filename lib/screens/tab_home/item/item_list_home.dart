import 'package:diemdanh/config/screens_name.dart';
import 'package:diemdanh/config_assets/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:diemdanh/config/function.dart';

// ignore: must_be_immutable
class ItemListHome extends StatelessWidget {
  final String? title;
  final String? icon;
  final int? index;
  final int? listDataLength;
  late double bottomRight = 0.0;
  late double bottomLeft = 0.0;
  late double topRight = 0.0;
  late double topLeft = 0.0;

  ItemListHome(
      {Key? key,
      required this.title,
      required this.icon,
      this.index,
      this.listDataLength})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (index == 0) {
      bottomRight = 0.0;
      bottomLeft = 0.0;
      topRight = 12.0;
      topLeft = 12.0;
    } else if (index == listDataLength) {
      bottomRight = 12.0;
      bottomLeft = 12.0;
      topRight = 0.0;
      topLeft = 0.0;
    } else {
      bottomRight = 0.0;
      bottomLeft = 0.0;
      topRight = 0.0;
      topLeft = 0.0;
    }
    return GestureDetector(
      onTap: onTap,
      child: Center(
        child: Container(
          margin: const EdgeInsets.fromLTRB(12, 0, 12, 0),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(topLeft),
                  topRight: Radius.circular(topRight),
                  bottomLeft: Radius.circular(bottomLeft),
                  bottomRight: Radius.circular(bottomRight)),
            ),
            padding: const EdgeInsets.fromLTRB(12, 12, 12, 0),
            child: Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        SvgPicture.asset(
                          icon ?? "",
                          width: 20,
                          height: 20,
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 20),
                          child: Text(title ?? ""),
                        ),
                      ],
                    ),
                    const Icon(
                      Icons.arrow_forward_ios,
                      color: Color(ColorAssets.colorBFC9DD),
                      size: 16.0,
                    ),
                  ],
                ),
                Container(
                    height: 1.0,
                    margin: const EdgeInsets.only(top: 12),
                    color: index == listDataLength? Colors.white : const Color(ColorAssets.colorF3F5F9),
                    child: null
                )
              ],
            ),
          ),
        ),
      )
    );
  }

  onTap (){
    switch (title) {
      case 'Xin phép nghỉ làm':
        Get.toNamed(NamedAppRoute.xinPhepNghiLam);
        break;
      default:
        popupOk("THONG_BAO".tr, "CHUC_NANG_DANG_PHAT_TRIEN".tr, null, null, null);
        break;
    }
  }
}
