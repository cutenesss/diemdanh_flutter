import 'package:diemdanh/api/call_api.dart';
import 'package:get/get.dart';

class TabHomeBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(CallApi());
    // Get.lazyPut(() => CallApi(), fenix: true);
  }
}