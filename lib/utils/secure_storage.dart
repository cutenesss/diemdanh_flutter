import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorage {
  static const _storage = FlutterSecureStorage();

  static String keyUser() => 'user';
  static String keyToken() => 'token';

  static Future saveObject(String key, String object) async =>
      await _storage.write(key: key, value: object);

  static Future<String?> getObject(String key) async =>
      await _storage.read(key: key);

  static Future setListObject(String key, List<String> listObjects) async {
    final value = json.encode(listObjects);
    await _storage.write(key: key, value: value);
  }

  static Future<List<String>?> getListObject(String key) async {
    final value = await _storage.read(key: key);
    return value == null ? null : List<String>.from(json.decode(value));
  }

  static Future deleteObject(String key) async{
    var deleteData = await _storage.delete(key: key);
    return deleteData;
  }
}
