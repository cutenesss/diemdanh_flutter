import 'package:dio/dio.dart';

abstract class ApiHelpers {
  void init(String token);

  Future<Response> getData(String url,  Map<String, String> data);
  Future<Response> postData(String url,  Map<String, String> data);
}