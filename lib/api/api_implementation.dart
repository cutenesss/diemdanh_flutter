import 'dart:convert';
import 'dart:io';
import 'package:diemdanh/config/setting.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'api_helpers.dart';

class ApiImplementation implements ApiHelpers {
  late Dio _dio;

  @override
  Future<Response> getData(String url, Map<String, String> data) async {
    // TODO: implement getRequest
    Response response;
    Options options = Options(
        headers: {HttpHeaders.acceptHeader: "accept: application/json"});
    try {
      response = await _dio.get(url, queryParameters: data, options: options);
    } on DioError catch (e) {
      debugPrint(e.message);
      throw Exception(e.message);
    }
    return response;
  }

  @override
  Future<Response> postData(String url, Map<String, String> data) async {
    // TODO: implement postData
    Response response;
    Options options =
        Options(headers: {HttpHeaders.contentTypeHeader: "application/json"});
    try {
      response = await _dio.post(url, data: jsonEncode(data), options: options);
    } on DioError catch (e) {
      debugPrint(e.message);
      throw Exception(e.message);
    }
    return response;
  }

  initializeInterceptors() {
    _dio.interceptors.add(InterceptorsWrapper(onError: (DioError e, handler) {
      debugPrint(e.message);
    }, onRequest: (options, handler) {
      debugPrint("${options.method} | ${options.path}");
    }, onResponse: (response, handler) {
      debugPrint(
          "${response.statusCode} ${response.statusMessage} ${response.data}");
    }));
  }

  @override
  void init(String token) {
    _dio = Dio(BaseOptions(
        baseUrl: rootUrl, headers: {"Authorization": "Bearer $token"}));
    initializeInterceptors();
  }
}
