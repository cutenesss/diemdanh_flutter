import 'package:json_annotation/json_annotation.dart';

part 'identified_device_info.g.dart';

@JsonSerializable()
class IdentifiedDeviceInfo{
  IdentifiedDeviceInfo();

  @JsonKey(name : "deviceId")
  String? deviceId;

  @JsonKey(name : "identifiedAt")
  String? identifiedAt;

  DateTime? get getIdentifiedAtDate => DateTime.tryParse(identifiedAt!);

  factory IdentifiedDeviceInfo.fromJson(Map<String, dynamic> json) => _$IdentifiedDeviceInfoFromJson(json);
  Map<String, dynamic> toJson() => _$IdentifiedDeviceInfoToJson(this);
}
