import 'package:json_annotation/json_annotation.dart';
import 'user_infomation.dart';

part 'user_information_response.g.dart';

@JsonSerializable()
class UserInformationResponse{
  UserInformationResponse();

  @JsonKey(name : "accessToken")
  String? token;

  @JsonKey(name : "user")
  UserInformation? user;

  factory UserInformationResponse.fromJson(Map<String, dynamic> json) => _$UserInformationResponseFromJson(json);
  Map<String, dynamic> toJson() => _$UserInformationResponseToJson(this);
}