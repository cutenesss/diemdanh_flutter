// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_response_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserResponseData _$UserResponseDataFromJson(Map<String, dynamic> json) {
  return UserResponseData()
    ..userInformationResponse = json['data'] == null
        ? null
        : UserInformationResponse.fromJson(
            json['data'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserResponseDataToJson(UserResponseData instance) =>
    <String, dynamic>{
      'data': instance.userInformationResponse,
    };
