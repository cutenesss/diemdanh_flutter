// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserProfile _$UserProfileFromJson(Map<String, dynamic> json) {
  return UserProfile()
    ..gender = json['gender'] as String?
    ..firstname = json['firstname'] as String?
    ..contactEmail = json['contactEmail'] as String?
    ..bankAccount = json['bankAccount'] as String?
    ..id = json['_id'] as String?
    ..phoneNumber = json['phoneNumber'] as String?
    ..lastname = json['lastname'] as String?
    ..placeOfBirth = json['placeOfBirth'] as String?
    ..team = json['team'] as String?
    ..updatedAt = json['updatedAt'] as String?
    ..dateOfBirth = json['dateOfBirth'] as String?
    ..username = json['username'] as String?
    ..baseSalary = json['baseSalary'] as int?;
}

Map<String, dynamic> _$UserProfileToJson(UserProfile instance) =>
    <String, dynamic>{
      'gender': instance.gender,
      'firstname': instance.firstname,
      'contactEmail': instance.contactEmail,
      'bankAccount': instance.bankAccount,
      '_id': instance.id,
      'phoneNumber': instance.phoneNumber,
      'lastname': instance.lastname,
      'placeOfBirth': instance.placeOfBirth,
      'team': instance.team,
      'updatedAt': instance.updatedAt,
      'dateOfBirth': instance.dateOfBirth,
      'username': instance.username,
      'baseSalary': instance.baseSalary,
    };
