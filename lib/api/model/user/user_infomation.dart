import 'package:json_annotation/json_annotation.dart';
import 'identified_device_info.dart';
import 'user_profile.dart';

part 'user_infomation.g.dart';

@JsonSerializable()
class UserInformation{
  UserInformation();

  @JsonKey(name : "_id")
  String? id;

  @JsonKey(name : "username")
  String? username;

  @JsonKey(name : "password")
  String? password;

  @JsonKey(name : "email")
  String? email;

  @JsonKey(name : "systemRoles")
  List<String>? systemRoles;

  @JsonKey(name : "identifiedDeviceInfo")
  IdentifiedDeviceInfo? identifiedDeviceInfo;

  @JsonKey(name : "profile")
  UserProfile? userProfile;

  factory UserInformation.fromJson(Map<String, dynamic> json) => _$UserInformationFromJson(json);
  Map<String, dynamic> toJson() => _$UserInformationToJson(this);
}
