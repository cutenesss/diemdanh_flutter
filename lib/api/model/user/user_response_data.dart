import 'package:json_annotation/json_annotation.dart';
import 'user_information_response.dart';

part 'user_response_data.g.dart';

@JsonSerializable()
class UserResponseData{
  UserResponseData();

  @JsonKey(name : "data")
  UserInformationResponse? userInformationResponse;

  factory UserResponseData.fromJson(Map<String, dynamic> json) => _$UserResponseDataFromJson(json);
  Map<String, dynamic> toJson() => _$UserResponseDataToJson(this);
}