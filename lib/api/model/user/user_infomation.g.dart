// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_infomation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInformation _$UserInformationFromJson(Map<String, dynamic> json) {
  return UserInformation()
    ..id = json['_id'] as String?
    ..username = json['username'] as String?
    ..password = json['password'] as String?
    ..email = json['email'] as String?
    ..systemRoles = (json['systemRoles'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList()
    ..identifiedDeviceInfo = json['identifiedDeviceInfo'] == null
        ? null
        : IdentifiedDeviceInfo.fromJson(
            json['identifiedDeviceInfo'] as Map<String, dynamic>)
    ..userProfile = json['profile'] == null
        ? null
        : UserProfile.fromJson(json['profile'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserInformationToJson(UserInformation instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'username': instance.username,
      'password': instance.password,
      'email': instance.email,
      'systemRoles': instance.systemRoles,
      'identifiedDeviceInfo': instance.identifiedDeviceInfo,
      'profile': instance.userProfile,
    };
