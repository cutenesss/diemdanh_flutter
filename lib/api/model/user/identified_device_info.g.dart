// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'identified_device_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IdentifiedDeviceInfo _$IdentifiedDeviceInfoFromJson(Map<String, dynamic> json) {
  return IdentifiedDeviceInfo()
    ..deviceId = json['deviceId'] as String?
    ..identifiedAt = json['identifiedAt'] as String?;
}

Map<String, dynamic> _$IdentifiedDeviceInfoToJson(
        IdentifiedDeviceInfo instance) =>
    <String, dynamic>{
      'deviceId': instance.deviceId,
      'identifiedAt': instance.identifiedAt,
    };
