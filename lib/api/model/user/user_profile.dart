import 'package:json_annotation/json_annotation.dart';

part 'user_profile.g.dart';

@JsonSerializable()
class UserProfile{
  UserProfile();

  @JsonKey(name : "gender")
  String? gender;

  @JsonKey(name : "firstname")
  String? firstname;

  @JsonKey(name : "contactEmail")
  String? contactEmail;

  @JsonKey(name : "bankAccount")
  String? bankAccount;

  @JsonKey(name : "_id")
  String? id;

  @JsonKey(name : "phoneNumber")
  String? phoneNumber;

  @JsonKey(name : "lastname")
  String? lastname;

  @JsonKey(name : "placeOfBirth")
  String? placeOfBirth;

  @JsonKey(name : "team")
  String? team;

  @JsonKey(name : "updatedAt")
  String? updatedAt;

  @JsonKey(name : "dateOfBirth")
  String? dateOfBirth;

  @JsonKey(name : "username")
  String? username;

  @JsonKey(name : "baseSalary")
  int? baseSalary;

  DateTime? get getUpdatedAtDate => DateTime.tryParse(updatedAt!);
  DateTime? get getDateOfBirthDate => DateTime.tryParse(dateOfBirth!);

  factory UserProfile.fromJson(Map<String, dynamic> json) => _$UserProfileFromJson(json);
  Map<String, dynamic> toJson() => _$UserProfileToJson(this);
}
