// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_information_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInformationResponse _$UserInformationResponseFromJson(
    Map<String, dynamic> json) {
  return UserInformationResponse()
    ..token = json['accessToken'] as String?
    ..user = json['user'] == null
        ? null
        : UserInformation.fromJson(json['user'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserInformationResponseToJson(
        UserInformationResponse instance) =>
    <String, dynamic>{
      'accessToken': instance.token,
      'user': instance.user,
    };
