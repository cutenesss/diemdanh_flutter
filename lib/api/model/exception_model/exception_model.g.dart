// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exception_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExceptionModel _$ExceptionModelFromJson(Map<String, dynamic> json) {
  return ExceptionModel()
    ..statusCode = json['statusCode'] as int?
    ..errorCode = json['errorCode'] as String?;
}

Map<String, dynamic> _$ExceptionModelToJson(ExceptionModel instance) =>
    <String, dynamic>{
      'statusCode': instance.statusCode,
      'errorCode': instance.errorCode,
    };
