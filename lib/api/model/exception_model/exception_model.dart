import 'package:json_annotation/json_annotation.dart';

part 'exception_model.g.dart';

@JsonSerializable()
class ExceptionModel{
  ExceptionModel();

  @JsonKey(name : "statusCode")
  int? statusCode;

  @JsonKey(name : "errorCode")
  String? errorCode;

  factory ExceptionModel.fromJson(Map<String, dynamic> json) => _$ExceptionModelFromJson(json);
  Map<String, dynamic> toJson() => _$ExceptionModelToJson(this);
}
