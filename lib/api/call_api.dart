import 'package:diemdanh/api/abstract_call_api.dart';
import 'package:diemdanh/api/api_helpers.dart';
import 'package:diemdanh/api/api_implementation.dart';
import 'package:diemdanh/api/url.dart';
import 'package:diemdanh/utils/secure_storage.dart';
import 'package:get/get.dart';

class CallApi implements AbstractCallApi {
  late ApiHelpers _apiHelpers;

  CallApi() {
    init();
  }

  void init () async {
    var token = await SecureStorage.getObject(SecureStorage.keyToken());
    _apiHelpers = Get.put(ApiImplementation());
    _apiHelpers.init(token ?? "");
  }
}
