import 'package:diemdanh/api/helper_api.dart';
import 'package:diemdanh/api/url.dart';
import 'package:dio/dio.dart';

class UserApi {
  Future login(Map<String, String> body) async {
    var response = await HelperApi.postData(Url.login, body);
    return response;
  }
}