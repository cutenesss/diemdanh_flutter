import 'dart:io';
import 'package:diemdanh/api/exception_handler.dart';
import 'package:diemdanh/api/model/exception_model/exception_model.dart';
import 'package:diemdanh/config/setting.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class HelperApi {
  static getData(String url, Map<String, String> data, bool isAuth) async {
    var headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };
    debugPrint('Api Get, url $url');
    var uri = Uri.https(rootUrl, url, data);
    // result -https://apiattendance.aisenote.com /dev/user ?username=a&password=b
    final responseJson;
    try {
      final response = await http.get(uri, headers: headers);
      responseJson = _returnResponse(response);
    } on SocketException {
      debugPrint('No net');
      throw FetchDataException('No Internet connection');
    }
    debugPrint('api get recieved!');
    return responseJson;
  }

  static postData(String url, Map<String, String> data) async {
    debugPrint('Api Post, url $url');
    var headers = {"Content-Type": "application/json"};
    // if (isAuth == true) {
    //   headers = {
    //  "Content-Type": "application/json",
    // 'Authorization': 'Bearer $token',
    //   };
    // }
    final encodeMessage = jsonEncode(data);
    var uri = Uri.https(rootUrl, url);
    final responseJson;
    try {
      final response =
      await http.post(uri, headers: headers, body: encodeMessage);
      responseJson = _returnResponse(response);
    } on SocketException {
      debugPrint('No net');
      throw FetchDataException('No Internet connection');
    }
    debugPrint('api post.');
    return responseJson;
  }

// Future<dynamic> put(String url, dynamic body) async {
//   debugPrint('Api Put, url $url');
//   var responseJson;
//   try {
//     final response = await http.put(_baseUrl + url, body: body);
//     responseJson = _returnResponse(response);
//   } on SocketException {
//     debugPrint('No net');
//     throw FetchDataException('No Internet connection');
//   }
//   debugPrint('api put.');
//   debugPrint(responseJson.toString());
//   return responseJson;
// }

// Future<dynamic> delete(String url) async {
//   debugPrint('Api delete, url $url');
//   var apiResponse;
//   try {
//     final response = await http.delete(_baseUrl + url);
//     apiResponse = _returnResponse(response);
//   } on SocketException {
//     debugPrint('No net');
//     throw FetchDataException('No Internet connection');
//   }
//   debugPrint('api delete.');
//   return apiResponse;
// }
}

_returnResponse(http.Response response) {
  Map<String, dynamic> data =
  Map<String, dynamic>.from(json.decode(response.body));
  switch (response.statusCode) {
    case 200:
      return data;
    case 201:
      return data;
    case 401:
      throw BadRequestException(ExceptionModel.fromJson(data).errorCode);
    case 403:
      throw UnauthorisedException(ExceptionModel.fromJson(data).errorCode);
    case 500:
      throw FetchDataException(
          'Đã có lỗi xảy ra. Vui lòng thử lại sau');
    default:
      throw FetchDataException(
          'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
  }
}
